package com.yhoc.hocgiaiphau.libdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DbBaiHocInfo extends SQLiteOpenHelper {
    public final String TAG_DBHOCINFO = "DbBaiHocInfo";
    public static final String DATABASE_NAME ="hocgiaiphau_db";

    private static final String TABLE_INFO = "info_table";

    private static final String info_table_ID = "id_info";
    private static final String info_table_NAME = "id_name";
    private static final String info_table_PATH = "id_path";
    private static final String info_table_COUNT = "id_count";


    private Context context;

    public DbBaiHocInfo(Context context) {
        super(context, DATABASE_NAME,null, 1);
        Log.d(TAG_DBHOCINFO, "Khoi tao database " + DATABASE_NAME);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlQueryBhocInfo = "CREATE TABLE "+TABLE_INFO +" (" +
                info_table_ID + " integer primary key, "+
                info_table_NAME + " TEXT, " + info_table_PATH + " TEXT, "+ info_table_COUNT + " INTEGER)";
        Log.d(TAG_DBHOCINFO, "Tao bang database DbBaiHocInfo: " + sqlQueryBhocInfo);
        db.execSQL(sqlQueryBhocInfo);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_INFO);
        onCreate(db);
        //Toast.makeText(context, "Drop list_bhoc_gphau successfylly", Toast.LENGTH_SHORT).show();
    }

    //Add new a student
    public void addBaiHocInfo(BaiHocInfo baihoc){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(info_table_NAME, baihoc.getNameBHocInfo());
        values.put(info_table_COUNT, baihoc.getSoLuongBHocInfo());
        values.put(info_table_PATH, baihoc.getDuongdanfileBHocInfo());
        //Neu de null thi khi value bang null thi loi
        db.insert(TABLE_INFO,null,values);
        db.close();
    }
    public BaiHocInfo getBHocInfoById(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_INFO, new String[] { info_table_ID,
                        info_table_NAME, info_table_COUNT, info_table_PATH}, info_table_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        BaiHocInfo baihoc = new BaiHocInfo(cursor.getInt(0),cursor.getString(1),cursor.getInt(2), cursor.getString(3));
        cursor.close();
        db.close();
        return baihoc;
    }
    public int Update(BaiHocInfo baihoc){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(info_table_NAME, baihoc.getNameBHocInfo());
        values.put(info_table_COUNT, baihoc.getSoLuongBHocInfo());
        values.put(info_table_PATH, baihoc.getDuongdanfileBHocInfo());
        return db.update(TABLE_INFO,values,info_table_ID +"=?",new String[] { String.valueOf(baihoc.getIdBHocInfo())});
    }
    public List<BaiHocInfo> getAllBHocInfo() {
        List<BaiHocInfo> listBaiHoc = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_INFO;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                BaiHocInfo baihoc = new BaiHocInfo();
                baihoc.setIdBHocInfo(cursor.getInt(0));
                baihoc.setNameBHocInfo(cursor.getString(1));
                baihoc.setSoLuongBHocInfo(cursor.getInt(3));
                baihoc.setDuongdanfileBHocInfo(cursor.getString(2));
                listBaiHoc.add(baihoc);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listBaiHoc;
    }
    public void deleteBHocInfo(BaiHocInfo baihoc) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INFO, info_table_ID + " = ?",
                new String[] { String.valueOf(baihoc.getIdBHocInfo()) });
        db.close();
    }
    public int getBHocInfoCount()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String countQuery = "SELECT * FROM " + TABLE_INFO;

            Cursor cursor = db.rawQuery(countQuery, null);
            if (!(cursor.moveToFirst()) || cursor.getCount() == 0) {
                cursor.close();
                return 0;
            } else {
                cursor.close();
                return cursor.getCount();
            }
        }catch (Exception ex){
            onCreate(db);
            return 0;
        }

    }
}

