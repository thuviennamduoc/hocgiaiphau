package com.yhoc.hocgiaiphau.libdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DbHocGiaiPhau extends SQLiteOpenHelper {
    public final String TAG_DBHOCGIAIPHAU = "DbHocGiaiPhau";
    public static final String DATABASE_NAME ="hocgiaiphau_db";



    private static final String TABLE_BAIHOC ="baihoc_table";

    private static final String baihoc_table_ID ="id_bhoc_gphau";
    private static final String baihoc_table_NAME ="name_bhoc_gphau";
    private static final String baihoc_table_COUNT = "count_bhoc_gphau";
    private static final String baihoc_table_PATH = "ddan_bhoc_gphau";


    private Context context;

    public DbHocGiaiPhau(Context context) {
        super(context, DATABASE_NAME,null, 1);
        Log.d("DbHocGiaiPhau", "DbHocGiaiPhau: ");
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlQueryBhocGPhau = "CREATE TABLE "+TABLE_BAIHOC +" (" +
                baihoc_table_ID +" integer primary key, "+
                baihoc_table_NAME + " TEXT," + baihoc_table_PATH + " TEXT, "+ baihoc_table_COUNT + " INTEGER)";
        db.execSQL(sqlQueryBhocGPhau);
        //Toast.makeText(context, "Create table list_bhoc_gphau successfylly", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_BAIHOC);
        onCreate(db);
        //Toast.makeText(context, "Drop list_bhoc_gphau successfylly", Toast.LENGTH_SHORT).show();
    }

    //Add new a student
    public void addBaiHocGiaiPhau(BaiHocGiaiPhau baihoc){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
       /* Log.i(TAG_DBHOCGIAIPHAU, "id: " + baihoc.getIdBHocGPhau()+  " name: " + baihoc.getNameBHocGPhau()
                + " path: " + baihoc.getDuongdanfileBHocGPhau() + " soluong: " + baihoc.getSoLuongBHocGPhau());*//* Log.i(TAG_DBHOCGIAIPHAU, "id: " + baihoc.getIdBHocGPhau()+  " name: " + baihoc.getNameBHocGPhau() */
        values.put(baihoc_table_NAME, baihoc.getNameBHocGPhau());
        values.put(baihoc_table_COUNT, baihoc.getSoLuongBHocGPhau());
        values.put(baihoc_table_PATH, baihoc.getDuongdanfileBHocGPhau());
        //Neu de null thi khi value bang null thi loi
        db.insert(TABLE_BAIHOC,null,values);
        db.close();
    }
    public BaiHocGiaiPhau getBHocGPhauById(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_BAIHOC, new String[] { baihoc_table_ID,
                        baihoc_table_NAME, baihoc_table_COUNT, baihoc_table_PATH}, baihoc_table_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        BaiHocGiaiPhau baihoc = new BaiHocGiaiPhau(cursor.getInt(0),cursor.getString(1),cursor.getInt(2), cursor.getString(3));
        cursor.close();
        db.close();
        return baihoc;
    }
    public int Update(BaiHocGiaiPhau baihoc){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(baihoc_table_ID, baihoc.getNameBHocGPhau());
        values.put(baihoc_table_NAME, baihoc.getSoLuongBHocGPhau());
        values.put(baihoc_table_PATH, baihoc.getDuongdanfileBHocGPhau());
        values.put(baihoc_table_COUNT, baihoc.getDuongdanfileBHocGPhau());
        return db.update(TABLE_BAIHOC,values,baihoc_table_ID +"=?",new String[] { String.valueOf(baihoc.getIdBHocGPhau())});
    }
    public List<BaiHocGiaiPhau> getAllBHocGPhau() {
        List<BaiHocGiaiPhau> listBaiHoc = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_BAIHOC;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                BaiHocGiaiPhau baihoc = new BaiHocGiaiPhau();
                baihoc.setIdBHocGPhau(cursor.getInt(0));
                baihoc.setNameBHocGPhau(cursor.getString(1));
                baihoc.setSoLuongBHocGPhau(cursor.getInt(3));
                baihoc.setDuongdanfileBHocGPhau(cursor.getString(2));
                listBaiHoc.add(baihoc);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listBaiHoc;
    }
    public void deleteBHocGPhau(BaiHocGiaiPhau baihoc) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BAIHOC, baihoc_table_ID + " = ?",
                new String[] { String.valueOf(baihoc.getIdBHocGPhau()) });
        db.close();
    }
    public int getBHocGPhauCount() {

        SQLiteDatabase db = this.getReadableDatabase();
        try{
            String countQuery = "SELECT * FROM " + TABLE_BAIHOC;
            Cursor cursor = db.rawQuery(countQuery, null);
            if (!(cursor.moveToFirst()) || cursor.getCount() == 0) {
                cursor.close();
                return 0;
            } else {
                cursor.close();
                return cursor.getCount();
            }
        }catch (Exception ex){
            onCreate(db);
            return 0;
        }
    }
}
