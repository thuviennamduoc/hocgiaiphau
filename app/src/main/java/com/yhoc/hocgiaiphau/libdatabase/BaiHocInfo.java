package com.yhoc.hocgiaiphau.libdatabase;

public class BaiHocInfo {
    private String nameBHocInfo;
    private int soLuongBHocInfo;
    private int idBHocInfo;
    private String duongdanfileBHocInfo;

    public BaiHocInfo(){

    }
    public BaiHocInfo(int idBHocInfo,String nameBHocInfo, int soLuongBHocInfo, String duongdanBHocInfo) {
        this.idBHocInfo = idBHocInfo;
        this.nameBHocInfo = nameBHocInfo;
        this.soLuongBHocInfo = soLuongBHocInfo;
        this.duongdanfileBHocInfo = duongdanBHocInfo;
    }

    public String getNameBHocInfo() {
        return nameBHocInfo;
    }

    public void setNameBHocInfo(String nameBHocInfo) {
        this.nameBHocInfo = nameBHocInfo;
    }

    public int getSoLuongBHocInfo() {
        return soLuongBHocInfo;
    }

    public void setSoLuongBHocInfo(int soLuongBHocInfo) {
        this.soLuongBHocInfo = soLuongBHocInfo;
    }

    public int getIdBHocInfo() {
        return idBHocInfo;
    }

    public void setIdBHocInfo(int idBHocInfo) {
        this.idBHocInfo = idBHocInfo;
    }

    public String getDuongdanfileBHocInfo() {
        return duongdanfileBHocInfo;
    }

    public void setDuongdanfileBHocInfo(String duongdanfileBHocInfo) {
        this.duongdanfileBHocInfo = duongdanfileBHocInfo;
    }
}
