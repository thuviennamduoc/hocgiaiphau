package com.yhoc.hocgiaiphau.libdatabase;

public class BaiHocGiaiPhau {
    private String nameBHocGPhau;
    private int soLuongBHocGPhau;
    private int idBHocGPhau;
    private String duongdanfileBHocGPhau;

    public String getDuongdanfileBHocGPhau() {
        return duongdanfileBHocGPhau;
    }

    public void setDuongdanfileBHocGPhau(String duongdanfileBHocGPhau) {
        this.duongdanfileBHocGPhau = duongdanfileBHocGPhau;
    }


    public BaiHocGiaiPhau(){

    }
    public BaiHocGiaiPhau(int id,String nameBHocGPhau, int soLuongBHocGPhau, String duongdanBHocGPhau) {
        this.idBHocGPhau = id;
        this.nameBHocGPhau = nameBHocGPhau;
        this.soLuongBHocGPhau = soLuongBHocGPhau;
        this.duongdanfileBHocGPhau = duongdanBHocGPhau;
    }

    public String getNameBHocGPhau() {
        return nameBHocGPhau;
    }

    public void setNameBHocGPhau(String nameBHocGPhau) {
        this.nameBHocGPhau = nameBHocGPhau;
    }



    public int getIdBHocGPhau() {
        return idBHocGPhau;
    }

    public void setIdBHocGPhau(int idBHocGPhau) {
        this.idBHocGPhau = idBHocGPhau;
    }

    public int getSoLuongBHocGPhau() {
        return soLuongBHocGPhau;
    }

    public void setSoLuongBHocGPhau(int soLuongBHocGPhau) {
        this.soLuongBHocGPhau = soLuongBHocGPhau;
    }
}
