package com.yhoc.hocgiaiphau;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.navigation.NavigationView;
import com.yhoc.hocgiaiphau.ui.bhocgphau.BHocGPhauDetailFragment;
import com.yhoc.hocgiaiphau.ui.home.HomeListBHocFragment;
import com.yhoc.hocgiaiphau.ui.home.ListBHocFragment;
import com.yhoc.hocgiaiphau.utils.ForceUpdateChecker;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BHocGPhauDetailFragment.TaskCallbacks, ForceUpdateChecker.OnUpdateNeededListener {
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;
    MainActivityViewModel mainactivitymodel;

    public static final String TAG_HGP_HomeFragment = "HGP_HomeFragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);
        navigationView = findViewById(R.id.nav_view);

        ForceUpdateChecker.with(this).onUpdateNeeded(this).check();

        setSupportActionBar(toolbar);
        //getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        mainactivitymodel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        if (savedInstanceState == null) {
            HomeListBHocFragment fragment = new HomeListBHocFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        Log.i(TAG_HGP_HomeFragment, "onCreate Main Activity");
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.i("TEST_LIFECYCLE", "onStart Activity");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("TEST_LIFECYCLE", "onRestart Activity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("TEST_LIFECYCLE", "onResume Activity");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_source:
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Nguồn");
                alertDialog.setMessage(getString(R.string.sourceBHocGPhau));
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            HomeListBHocFragment fragment = new HomeListBHocFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, fragment).addToBackStack(null).commit();
        } else if (id == R.id.nav_myortherapp) {
            String appPackageName = "com.AnatomyLearning.Anatomy3DViewer3&hl=vi";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        } else if (id == R.id.nav_ortherapp) {
            //final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            String appPackageName = "com.AnatomyLearning.Anatomy3DViewer3&hl=vi";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        } else if (id == R.id.nav_basic) {
            ListBHocFragment lishbhocfragment = new ListBHocFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, lishbhocfragment).addToBackStack(null).commit();
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void updateUI() {
        // Tap to pdfView then hide or unhide Navigation drawer
        if (getSupportActionBar().isShowing() == true) {
            getSupportActionBar().hide();
        } else {
            getSupportActionBar().show();
        }
    }

    @Override
    public void onUpdateNeeded(final String updateUrl, final boolean forceUpdate) {
        AlertDialog dialog = new AlertDialog.Builder(this, R.style.DialogTheme)
                .setTitle(R.string.dialog_update_title)
                .setMessage(R.string.dialog_update_msg)
                .setCancelable(!forceUpdate)
                .setPositiveButton(R.string.update,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        })
                .setNegativeButton(R.string.no_thanks,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (forceUpdate)
                                    finish();
                            }
                        })
                .create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        if (updateUrl.contains("play.google.com/store/apps/details?id=")) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + updateUrl.substring(updateUrl.indexOf("=") + 1))));
            } catch (android.content.ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl)));
            }
        } else {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
