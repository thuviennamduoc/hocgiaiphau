package com.yhoc.hocgiaiphau.lib;

public class ListDataItem {
    private String nameFile;
    private String namePath;

    public int getIdDataItem() {
        return idDataItem;
    }

    public void setIdDataItem(int idDataItem) {
        this.idDataItem = idDataItem;
    }

    private int idDataItem;

    public String getNameFile() {
        return nameFile;
    }

    public void setNameFile(String nameFile) {
        this.nameFile = nameFile;
    }

    public String getNamePath() {
        return namePath;
    }

    public void setNamePath(String namePath) {
        this.namePath = namePath;
    }
    public ListDataItem(String name, String path, int id){
        this.nameFile = name;
        this.namePath = path;
        this.idDataItem = id;
    }

}
