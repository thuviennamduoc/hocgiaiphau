package com.yhoc.hocgiaiphau.lib;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.libdatabase.BaiHocInfo;

import java.util.List;

public class BaiHocInfoAdapter extends ArrayAdapter<BaiHocInfo> {

    private Context context;
    private int resource;
    private List<BaiHocInfo> arrBHocInfo;

    public BaiHocInfoAdapter(Context context, int resource, List<BaiHocInfo> arrBaiHocInfo)
    {
        super(context, resource, arrBaiHocInfo);
        this.context = context;
        this.resource = resource;
        this.arrBHocInfo = arrBaiHocInfo;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            BaiHocInfoAdapter.ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(resource, parent, false);
                viewHolder = new BaiHocInfoAdapter.ViewHolder();
                viewHolder.tvName = (TextView) convertView.findViewById(R.id.tvListItemTabInfo);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (BaiHocInfoAdapter.ViewHolder) convertView.getTag();
            }
            BaiHocInfo baihoc = arrBHocInfo.get(position);
            viewHolder.tvName.setText(baihoc.getNameBHocInfo());
        }catch (Exception ex){
            Log.e("Errr", ex.toString());
        }
        return convertView;
    }

    public class ViewHolder {
        TextView tvName;
        ImageView imgIcon;

    }
}

