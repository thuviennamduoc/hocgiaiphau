package com.yhoc.hocgiaiphau.lib;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.libdatabase.BaiHocInfo;

import java.util.List;

public class BHocInfoRecycleAdapter extends RecyclerView.Adapter<BHocInfoRecycleAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    private List<BaiHocInfo> arrBHocInfo;
    private ItemClickListener mClickListener;

    public BHocInfoRecycleAdapter(Context context, List<BaiHocInfo> mlistbhocinfo) {
        this.mInflater = LayoutInflater.from(context);
        this.arrBHocInfo = mlistbhocinfo;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_views_tabinfo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String namebhoc = arrBHocInfo.get(position).getNameBHocInfo();
        holder.myTextView.setText(namebhoc);
    }
    @Override
    public int getItemCount() {
        return arrBHocInfo.size();
    }
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        public ViewHolder(View itemView){
            super(itemView);
            myTextView = itemView.findViewById(R.id.tvListItemTabInfo);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            if(mClickListener!=null){
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }
}
