package com.yhoc.hocgiaiphau.lib;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListData {
    public static HashMap<String, List<ListDataItem>> getData() {
        HashMap<String, List<ListDataItem>> expandableListDetail = new HashMap<String, List<ListDataItem>>();

        List<ListDataItem> hexuongkhop = new ArrayList<ListDataItem>();
        hexuongkhop.add(new ListDataItem("Cơ bản", "hexuongkhop_coban.pdf", 1));
        hexuongkhop.add(new ListDataItem("Nâng cao", "hexuongkhop_nangcao.pdf", 2));
        hexuongkhop.add(new ListDataItem("Bài Học", "", 3));

        List<ListDataItem> heco = new ArrayList<ListDataItem>();
        heco.add(new ListDataItem("Cơ bản", "heco_coban.pdf", 4));
        heco.add(new ListDataItem("Nâng cao", "heco_nangcao.pdf", 5));
        heco.add(new ListDataItem("Bài Học", "", 6));

        List<ListDataItem> hetuanhoan = new ArrayList<ListDataItem>();
        hetuanhoan.add(new ListDataItem("Cơ bản", "hetuanhoan_coban.pdf", 7));
        hetuanhoan.add(new ListDataItem("Nâng cao", "hetuanhoan_nangcao.pdf", 8));
        hetuanhoan.add(new ListDataItem("Bài Học", "", 9));

        List<ListDataItem> hehohap = new ArrayList<ListDataItem>();
        hehohap.add(new ListDataItem("Cơ bản", "hehohap_coban.pdf", 10));
        hehohap.add(new ListDataItem("Nâng cao", "hehohap_nangcao.pdf", 11));
        hehohap.add(new ListDataItem("Bài Học", "", 12));

        List<ListDataItem> hetieuhoa = new ArrayList<ListDataItem>();
        hetieuhoa.add(new ListDataItem("Cơ bản", "hetieuhoa_coban.pdf", 13));
        hetieuhoa.add(new ListDataItem("Nâng cao", "hetieuhoa_nangcao.pdf", 14));
        hetieuhoa.add(new ListDataItem("Bài Học", "", 15));

        List<ListDataItem> hetietnieu = new ArrayList<ListDataItem>();
        hetietnieu.add(new ListDataItem("Cơ bản", "hetietnieu_coban.pdf", 16));
        hetietnieu.add(new ListDataItem("Nâng cao", "hetietnieu_nangcao.pdf", 17));
        hetietnieu.add(new ListDataItem("Bài Học", "", 18));


        List<ListDataItem> hethankinh = new ArrayList<ListDataItem>();
        hethankinh.add(new ListDataItem("Cơ bản", "hethankinh_coban.pdf", 19));
        hethankinh.add(new ListDataItem("Nâng cao", "hethankinh_nangcao.pdf", 20));
        hethankinh.add(new ListDataItem("Bài Học", "", 21));

        List<ListDataItem> henoitiet = new ArrayList<ListDataItem>();
        henoitiet.add(new ListDataItem("Cơ bản", "henoitiet_coban.pdf", 22));
        henoitiet.add(new ListDataItem("Nâng cao", "henoitiet_nangcao.pdf", 23));
        henoitiet.add(new ListDataItem("Bài Học", "", 24));


        expandableListDetail.put("Hệ xương khớp", hexuongkhop);
        expandableListDetail.put("Hệ cơ", heco);
        expandableListDetail.put("Hệ tuần hoàn", hetuanhoan);
        expandableListDetail.put("Hệ hô hấp", hehohap);
        expandableListDetail.put("Hệ tiêu hóa", hetieuhoa);
        expandableListDetail.put("Hệ tiết niệu", hetietnieu);
        expandableListDetail.put("Hệ thần kinh", hethankinh);
        expandableListDetail.put("Hệ nội tiết", henoitiet);

        return expandableListDetail;
    }
}
