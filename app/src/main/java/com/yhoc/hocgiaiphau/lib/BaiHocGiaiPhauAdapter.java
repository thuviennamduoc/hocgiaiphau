package com.yhoc.hocgiaiphau.lib;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.libdatabase.BaiHocGiaiPhau;

import java.util.List;

public class BaiHocGiaiPhauAdapter extends ArrayAdapter<BaiHocGiaiPhau> {

    private Context context;
    private int resource;
    private List<BaiHocGiaiPhau> arrBHocGPhau;

    public BaiHocGiaiPhauAdapter(Context context, int resource, List<BaiHocGiaiPhau> arrBaiHocGiaiPhau)
    {
        super(context, resource, arrBaiHocGiaiPhau);
        this.context = context;
        this.resource = resource;
        this.arrBHocGPhau = arrBaiHocGiaiPhau;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(context).inflate(resource, parent, false);
                viewHolder = new ViewHolder();
                viewHolder.tvName = (TextView) convertView.findViewById(R.id.textViewListItem);
                //viewHolder.imgIcon = (ImageView) convertView.findViewById(R.id.imgViewListItem);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            BaiHocGiaiPhau baihoc = arrBHocGPhau.get(position);
            /*if (baihoc.getSoLuongBHocGPhau() > 0) {
                viewHolder.imgIcon.setVisibility(View.VISIBLE);
            } else {
                viewHolder.imgIcon.setVisibility(View.INVISIBLE);
            }*/
            viewHolder.tvName.setText(baihoc.getNameBHocGPhau());
        }catch (Exception ex){
            Log.e("Errr", ex.toString());
        }
        return convertView;
    }

    public class ViewHolder {
        TextView tvName;
        //ImageView imgIcon;

    }
}

