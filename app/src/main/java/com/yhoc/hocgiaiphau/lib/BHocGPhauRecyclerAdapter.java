package com.yhoc.hocgiaiphau.lib;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yhoc.hocgiaiphau.R;

import androidx.recyclerview.widget.RecyclerView;

import com.yhoc.hocgiaiphau.libdatabase.BaiHocGiaiPhau;

import java.util.List;

public class BHocGPhauRecyclerAdapter extends RecyclerView.Adapter<BHocGPhauRecyclerAdapter.ViewHolder> {
    List<BaiHocGiaiPhau> listbhocgphau;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public BHocGPhauRecyclerAdapter(Context context, List<BaiHocGiaiPhau> mlistbhocgphau) {
        this.mInflater = LayoutInflater.from(context);
        this.listbhocgphau = mlistbhocgphau;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_views, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String namebhoc = listbhocgphau.get(position).getNameBHocGPhau();
        holder.myTextView.setText(namebhoc);
    }

    @Override
    public int getItemCount() {
        return listbhocgphau.size();
    }
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
    String getItem(int i) {
        return listbhocgphau.get(i).getNameBHocGPhau();
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        public ViewHolder(View itemView){
                super(itemView);
                myTextView = itemView.findViewById(R.id.textViewListItem);
                itemView.setOnClickListener(this);
            }
        @Override
        public void onClick(View view) {
            if(mClickListener!=null){
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

}
