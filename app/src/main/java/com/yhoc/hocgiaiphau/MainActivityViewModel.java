package com.yhoc.hocgiaiphau;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class MainActivityViewModel extends AndroidViewModel {
    public MutableLiveData<Integer> getmCount() {
        return mCount;
    }

    public void setmCount(MutableLiveData<Integer> mCount) {
        this.mCount = mCount;
    }

    private MutableLiveData<Integer> mCount;

    public MainActivityViewModel(Application application) {
        super(application);
        mCount = new MutableLiveData<>();
    }

    public int getValueFromCount() {
        return mCount.getValue();
    }

    public void setValueForCountPlus() {
        int count = getValueFromCount();
        this.mCount.setValue(count + 1);
    }

    public void setValueForCountToZero() {
        this.mCount.setValue(0);
    }


}
