package com.yhoc.hocgiaiphau.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

public class ForceUpdateChecker {

    private static final String TAG = ForceUpdateChecker.class.getSimpleName();

    public static final String KEY_FORCE_UPDATE_REQUIRED = "force_update_required";
    public static final String KEY_NEWEST_VERSION_CODE = "newest_version_code";
    public static final String KEY_UPDATE_URL = "store_url";

    private OnUpdateNeededListener onUpdateNeededListener;
    private Context context;

    public interface OnUpdateNeededListener {
        void onUpdateNeeded(String updateUrl, boolean forceUpdate);
    }

    public static Builder with(@NonNull Context context) {
        return new Builder(context);
    }

    private ForceUpdateChecker(@NonNull Context context,
                               OnUpdateNeededListener onUpdateNeededListener) {
        this.context = context;
        this.onUpdateNeededListener = onUpdateNeededListener;
    }

    private void check() {
        final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();
        remoteConfig.fetch(60).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                long newestVersion = remoteConfig.getLong(KEY_NEWEST_VERSION_CODE);
                long appVersion = getAppVersion(context);
                String updateUrl = remoteConfig.getString(KEY_UPDATE_URL);

                if (newestVersion > appVersion
                        && onUpdateNeededListener != null) {
                    onUpdateNeededListener.onUpdateNeeded(updateUrl, remoteConfig.getBoolean(KEY_FORCE_UPDATE_REQUIRED));
                }
            }
        });

    }

    private long getAppVersion(Context context) {
        long result = 0;

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                result = context.getPackageManager()
                        .getPackageInfo(context.getPackageName(), 0)
                        .getLongVersionCode();
            } else
                result = context.getPackageManager()
                        .getPackageInfo(context.getPackageName(), 0)
                        .versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }

        return result;
    }

    public static class Builder {

        private Context context;
        private OnUpdateNeededListener onUpdateNeededListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder onUpdateNeeded(OnUpdateNeededListener onUpdateNeededListener) {
            this.onUpdateNeededListener = onUpdateNeededListener;
            return this;
        }

        public ForceUpdateChecker build() {
            return new ForceUpdateChecker(context, onUpdateNeededListener);
        }

        public ForceUpdateChecker check() {
            ForceUpdateChecker forceUpdateChecker = build();
            forceUpdateChecker.check();

            return forceUpdateChecker;
        }
    }
}
