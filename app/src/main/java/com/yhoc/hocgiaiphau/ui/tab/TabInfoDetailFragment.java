package com.yhoc.hocgiaiphau.ui.tab;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.yhoc.hocgiaiphau.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.shockwave.pdfium.PdfDocument;

import java.util.List;

public class TabInfoDetailFragment extends Fragment implements OnPageChangeListener, OnLoadCompleteListener {
    public String SAMPLE_FILE = "coban_hetieuhoa.pdf";
    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;
    private TabInfoDetailViewModel tabinfodetailmodel;

    private static final String ARG_TABINFODETAILFRAGMENT = "TabInfoDetailFragment";
    private static final String KEY_TABINFODETAILFRAGMENT_TENFILE = "key_tabinfodetail_tenfile";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabinfo_detail, container, false);

        tabinfodetailmodel = ViewModelProviders.of(requireActivity()).get(TabInfoDetailViewModel.class);
        //tabBasicViewModel = new TabBasicViewModel();
        pdfFileName = tabinfodetailmodel.layGiaTriTenFile() + ".pdf";
        pdfView= root.findViewById(R.id.pdfViewTabInfoDetail);
        Log.i(ARG_TABINFODETAILFRAGMENT, "Doc xong ten File" + pdfFileName);
        displayFromAsset(pdfFileName);
        return root;
    }
    private void displayFromAsset(String assetFileName) {
        pdfView.fromAsset(assetFileName)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onRender(new OnRenderListener() {
                    @Override
                    public void onInitiallyRendered(int nbPages) {
                        Log.i(ARG_TABINFODETAILFRAGMENT, "aaaaaaaa");
                        pdfView.fitToWidth(0);
                    }
                })
                .onLoad(this)
                .load();


    }
    /*@Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation ==Configuration.ORIENTATION_LANDSCAPE)
        {
            pdfView.fitToWidth(1);
        }
    }*/
    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        //getActivity().setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
        Toast.makeText(this.getActivity(), String.format("%s %s / %s", pdfFileName, page + 1, pageCount), Toast.LENGTH_LONG);
        Log.i(ARG_TABINFODETAILFRAGMENT, String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }
    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {
            Log.e(ARG_TABINFODETAILFRAGMENT, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));
            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }
}
