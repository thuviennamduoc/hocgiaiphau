package com.yhoc.hocgiaiphau.ui.tab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.lib.BHocInfoRecycleAdapter;
import com.yhoc.hocgiaiphau.lib.BaiHocInfoAdapter;
import com.yhoc.hocgiaiphau.libdatabase.BaiHocInfo;
import com.yhoc.hocgiaiphau.libdatabase.DbBaiHocInfo;

import java.util.List;

public class TabInfoListFragment extends Fragment implements BHocInfoRecycleAdapter.ItemClickListener{

    private RecyclerView rcvListInfo;
    BHocInfoRecycleAdapter adapterRCVBHocInfo;

    private ListView lvlistInfo;
    private TextView tv;
    List<BaiHocInfo> arrBaiHocInfo;
    private TabInfoDetailViewModel tabinfodetailmodel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabinfo_list, container, false);
        //lvlistInfo = root.findViewById(R.id.lv_tabinfo_list);
        rcvListInfo = root.findViewById(R.id.lv_tabinfo_list);


        init();
        initLRecycleViewListInfo();
        return root;
    }
    public void initLRecycleViewListInfo(){
        rcvListInfo.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterRCVBHocInfo = new BHocInfoRecycleAdapter(getContext(), arrBaiHocInfo);
        adapterRCVBHocInfo.setClickListener(this);
        rcvListInfo.setAdapter(adapterRCVBHocInfo);
    }
    @Override
    public void onItemClick(View view, int position) {
        tabinfodetailmodel = ViewModelProviders.of(requireActivity()).get(TabInfoDetailViewModel.class);
        tabinfodetailmodel.setGiaTriTenFile(arrBaiHocInfo.get(position).getDuongdanfileBHocInfo());

        TabInfoDetailFragment tabinfolist_fragment = new TabInfoDetailFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_tabinfo, tabinfolist_fragment).addToBackStack(null).commit();
    }
    public void initListViewListInfo(){
        BaiHocInfoAdapter baihocAdapter = new BaiHocInfoAdapter(this.getActivity(), R.layout.list_item_views_tabinfo,arrBaiHocInfo);
        lvlistInfo.setAdapter(baihocAdapter);

        lvlistInfo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                tabinfodetailmodel = ViewModelProviders.of(requireActivity()).get(TabInfoDetailViewModel.class);
                tabinfodetailmodel.setGiaTriTenFile(arrBaiHocInfo.get(i).getDuongdanfileBHocInfo());

                TabInfoDetailFragment tabinfolist_fragment = new TabInfoDetailFragment();
                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_tabinfo, tabinfolist_fragment).addToBackStack(null).commit();
            }
        });
    }
    public void init(){
        DbBaiHocInfo dbBInfoManager = new DbBaiHocInfo(getActivity());
        int soluong = dbBInfoManager.getBHocInfoCount();
        if(soluong==0){
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(1,"Cơ bản Hệ tiêu hóa", 0, "baihoc_hetieuhoa"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(2,"Cơ bản Hệ hô hấp", 0, "baihoc_hehohap"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(3,"Cơ bản Hệ tim mạch", 0, "baihoc_hetimmach"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(4,"Cơ bản Hệ thần kinh", 0, "baihoc_hethankinh"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(5,"Cơ bản Hệ xương khớp", 0, "baihoc_hexuongkhop"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(6,"12 Đôi dây thần kinh sọ", 0, "gp_12doidaythanhkinhso"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(7,"Giải phẩu dạ dày", 0, "gp_daday"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(8,"Ruột non", 0, "gp_ruotnon"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(9,"Đại tràng - Ruột già", 0, "gp_daitrang"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(10,"Gan", 0, "gp_gan"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(11,"Tụy", 0, "gp_tuy"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(12,"Hệ tĩnh mạch cửa", 0, "gp_hetinhmachcua"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(13,"Thận", 0, "gp_than"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(14,"Thận full", 0, "gp_thandaydu"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(15,"Khoang mũi - hầu - thanh quản", 0, "gp_khoangmuihauthanhquan"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(16,"Giải phẩu tim mạch", 0, "gp_timmach"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(17,"Trung thất", 0, "gp_trungthat"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(18,"Mô thần kinh", 0, "gp_mothankinh"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(19,"Đại cương về xương", 0, "gp_daicuongvexuong"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(20,"Xương đầu mặt", 0, "gp_xuongdaumat"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(21,"Cơ vân", 0, "gp_covan"));
            dbBInfoManager.addBaiHocInfo(new BaiHocInfo(22,"Phúc mạc", 0, "gp_phucmac"));

        }
        arrBaiHocInfo = dbBInfoManager.getAllBHocInfo();
    }


}
