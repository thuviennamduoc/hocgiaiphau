package com.yhoc.hocgiaiphau.ui.tab;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TabPreViewModel extends ViewModel {
    private MutableLiveData<String> tenFile;
    public TabPreViewModel(){
        this.tenFile = new MutableLiveData<>();
    }

    public void setGiaTriTenFile (String giatri){
        tenFile.setValue(giatri);
    }

    public MutableLiveData<String> getTenFile() {
        return tenFile;
    }

    public void setTenFile(MutableLiveData<String> tenFile) {
        this.tenFile = tenFile;
    }

    public String layGiaTriTenFile(){
        return tenFile.getValue();
    }
}
