package com.yhoc.hocgiaiphau.ui.tab;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.github.barteksc.pdfviewer.listener.OnTapListener;
import com.yhoc.hocgiaiphau.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.shockwave.pdfium.PdfDocument;

import java.util.List;

public class TabBasicFragment extends Fragment implements OnPageChangeListener, OnLoadCompleteListener {
    public String SAMPLE_FILE = "coban_hetieuhoa.pdf";
    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;

    private static final String ARG_TABBASICFRAGMENT = "TabBasicFragment";
    private static final String KEY_TABBASICFRAGMENT_TENFILE = "key_tabbasicfragment_tenfile";
    private TaskCallbacks mCallbacks;

    public interface TaskCallbacks {
        void updateUI();
    }

    private TabBasicViewModel tabBasicViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*tabBasicViewModel = ViewModelProviders.of(this).get(TabBasicViewModel.class);
        int index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        tabBasicViewModel.setGiaTri(index);*/
    }
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabbasic, container, false);

        tabBasicViewModel = ViewModelProviders.of(requireActivity()).get(TabBasicViewModel.class);
        //tabBasicViewModel = new TabBasicViewModel();
        pdfFileName = tabBasicViewModel.layGiaTriTenFile() + "_coban.pdf";
        pdfView= root.findViewById(R.id.pdfViewTabBasic);
        Log.i(ARG_TABBASICFRAGMENT, "Doc xong ten File" + pdfFileName);
        displayFromAsset(pdfFileName);

        Log.i("TEST_LIFECYCLE", "onCreateView Fragment TabBasic");
        return root;
    }
    @Override
    public void onStart() {
        super.onStart();
        Log.i("TEST_LIFECYCLE", "onStart Fragment TabBasic");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("TEST_LIFECYCLE", "onResume Fragment TabBasic");
    }

    private void displayFromAsset(String assetFileName) {
        pdfView.fromAsset(assetFileName)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .enableDoubletap(true)
                .onTap(new OnTapListener() {
                    @Override
                    public boolean onTap(MotionEvent e) {
                        Activity activity = getActivity();
                        mCallbacks = (TaskCallbacks) activity;
                        mCallbacks.updateUI();
                        Log.i("TEST_LIFECYCLE", "Tapping to pdf");
                        return true;
                    }
                })
                .onRender(new OnRenderListener() {
                    @Override
                    public void onInitiallyRendered(int nbPages) {
                        Log.i("TEST_LIFECYCLE", "Landscape mode actived");
                        pdfView.fitToWidth(0);
                    }
                })
                .onLoad(this)
                .load();


    }
    /*@Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation ==Configuration.ORIENTATION_LANDSCAPE)
        {
            pdfView.fitToWidth(0);
        }
    }*/
    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        //getActivity().setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
        Toast.makeText(this.getActivity(), String.format("%s %s / %s", pdfFileName, page + 1, pageCount), Toast.LENGTH_LONG);
        Log.i(ARG_TABBASICFRAGMENT, String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }
    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {
            Log.e(ARG_TABBASICFRAGMENT, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));
            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }
}
