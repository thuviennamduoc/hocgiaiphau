package com.yhoc.hocgiaiphau.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.lib.BHocInfoRecycleAdapter;
import com.yhoc.hocgiaiphau.lib.BaiHocInfoAdapter;
import com.yhoc.hocgiaiphau.libdatabase.BaiHocInfo;
import com.yhoc.hocgiaiphau.libdatabase.DbBaiHocInfo;
import com.yhoc.hocgiaiphau.ui.bhocgphau.BHocGPhauDetailFragment;
import com.yhoc.hocgiaiphau.ui.bhocgphau.BHocGPhauDetailViewModel;

import java.util.ArrayList;
import java.util.List;

public class ListBHocFragment extends Fragment implements BHocInfoRecycleAdapter.ItemClickListener{

    private RecyclerView rcvListInfo;
    BHocInfoRecycleAdapter adapterRCVBHocInfo;

    private TextView tv;
    List<BaiHocInfo> arrBaiHocInfo;
    private BHocGPhauDetailViewModel bhocgphaudetailmodel;
    public static final String TAG_HGP_ListBHocFragment = "HGP_ListBHocFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_listbhoc, container, false);
        rcvListInfo = root.findViewById(R.id.lv_listbhoc);


        initList();
        initLRecycleViewListInfo();

        Log.i(TAG_HGP_ListBHocFragment, "onCreateView ListBHocFragment");
        return root;
    }
    public void initLRecycleViewListInfo(){
        rcvListInfo.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterRCVBHocInfo = new BHocInfoRecycleAdapter(getContext(), arrBaiHocInfo);
        adapterRCVBHocInfo.setClickListener(this);
        rcvListInfo.setAdapter(adapterRCVBHocInfo);
    }
    @Override
    public void onItemClick(View view, int position) {
        bhocgphaudetailmodel = ViewModelProviders.of(requireActivity()).get(BHocGPhauDetailViewModel.class);

        String nameFile = arrBaiHocInfo.get(position).getDuongdanfileBHocInfo();
        bhocgphaudetailmodel.setValueForNameFile(nameFile);

        BHocGPhauDetailFragment bhocgphaudetail_fragment = new BHocGPhauDetailFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, bhocgphaudetail_fragment).addToBackStack(null).commit();

    }
    public void initList()
    {
        arrBaiHocInfo = new ArrayList<>();
        arrBaiHocInfo.add(new BaiHocInfo(1,"Cơ bản Hệ tiêu hóa", 0, "baihoc_hetieuhoa.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(2,"Cơ bản Hệ hô hấp", 0, "baihoc_hehohap.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(3,"Cơ bản Hệ tim mạch", 0, "baihoc_hetimmach.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(4,"Cơ bản Hệ thần kinh", 0, "baihoc_hethankinh.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(5,"Cơ bản Hệ xương khớp", 0, "baihoc_hexuongkhop.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(6,"12 Đôi dây thần kinh sọ", 0, "gp_12doidaythankinhso.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(7,"Giải phẩu dạ dày", 0, "gp_daday.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(8,"Ruột non", 0, "gp_ruotnon.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(9,"Đại tràng - Ruột già", 0, "gp_daitrang.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(10,"Gan", 0, "gp_gan.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(11,"Tụy", 0, "gp_tuy.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(12,"Hệ tĩnh mạch cửa", 0, "gp_hetinhmachcua.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(13,"Thận", 0, "gp_than.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(14,"Thận full", 0, "gp_thandaydu.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(15,"Khoang mũi - hầu - thanh quản", 0, "gp_khoangmuihauthanhquan.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(16,"Giải phẩu tim mạch", 0, "gp_timmach.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(17,"Trung thất", 0, "gp_trungthat.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(18,"Mô thần kinh", 0, "gp_mothankinh.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(19,"Đại cương về xương", 0, "gp_daicuongvexuong.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(20,"Xương đầu mặt", 0, "gp_xuongdaumat.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(21,"Cơ vân", 0, "gp_covan.pdf"));
        arrBaiHocInfo.add(new BaiHocInfo(22,"Phúc mạc", 0, "gp_phucmac.pdf"));

    }
}

