package com.yhoc.hocgiaiphau.ui.bhocgphau;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.yhoc.hocgiaiphau.MainActivityViewModel;
import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.ui.tab.TabBasicViewModel;
import com.yhoc.hocgiaiphau.ui.tab.TabPreViewModel;
import com.yhoc.hocgiaiphau.ui.tab.TabsSectionAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.tabs.TabLayout;

public class BHocGPhauFragment extends Fragment {

    private BHocGPhauViewModel bhocgiaiphauViewModel;
    public static final String KEY_BHocGPhau = "KEY_BHocGPhau";
    public static final String KEY_TENFILE_ITEM = "key_tenfile_item";
    TabsSectionAdapter tabsectionApdapter;
    ViewPager viewPager;
    TabLayout tabs;


    TabBasicViewModel tabbasicviewmodel;
    TabPreViewModel tabpreviewmodel;
    MainActivityViewModel mainviewmodel;
    private AdView mAdView;

    String tenFile = "baihoc_hethankinh.pdf";
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        bhocgiaiphauViewModel =
                ViewModelProviders.of(this).get(BHocGPhauViewModel.class);
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);
        /*bhocgiaiphauViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            tenFile = bundle.getString(KEY_TENFILE_ITEM);
        }
        tabbasicviewmodel = ViewModelProviders.of(requireActivity()).get(TabBasicViewModel.class);
        tabpreviewmodel = ViewModelProviders.of(requireActivity()).get(TabPreViewModel.class);
        mainviewmodel = ViewModelProviders.of(requireActivity()).get(MainActivityViewModel.class);


        tabbasicviewmodel.setGiaTriTenFile(tenFile);
        tabpreviewmodel.setGiaTriTenFile(tenFile);

        tabsectionApdapter = new TabsSectionAdapter(getActivity(), getActivity().getSupportFragmentManager());

        Log.i(KEY_BHocGPhau,"Goi duoc ham oncreateview"  + tenFile);
        tabsectionApdapter.setTenFile(tenFile);
        viewPager = root.findViewById(R.id.view_pager);
        //TabLayout tabLayout = root.findViewById(R.id.tabs);
        //tabLayout.setupWithViewPager(viewPager, true);

        //sectionsPagerAdapter.notifyDataSetChanged();
        viewPager.setAdapter(tabsectionApdapter);


        if(savedInstanceState!=null){
            //int tabindex = mainviewmodel.layGiaTriTabIndex();
            //viewPager.setCurrentItem(tabindex);
        }

        //tabs = root.findViewById(R.id.tabs);
        //tabs.setupWithViewPager(viewPager);


        MobileAds.initialize(getContext(), getContext().getString(R.string.appmob_id));
        AdView mAdView = root.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        Log.i("TEST_LIFECYCLE", "onCreateView Fragment Hoc");

        return root;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        int tabIndex = viewPager.getCurrentItem();

        Log.i("TEST_LIFECYCLE", "set value onSaveInstanceState: ");

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("TEST_LIFECYCLE", "onStart Fragment BHoc");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("TEST_LIFECYCLE", "onResume Fragment BHoc");
    }
}
