package com.yhoc.hocgiaiphau.ui.tab;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.yhoc.hocgiaiphau.R;

public class TabInfoFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tabinfo, container, false);

        TabInfoListFragment tabinfolist_fragment = new TabInfoListFragment();

        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_tabinfo, tabinfolist_fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();



        //FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.replace(R.id.frame_tabinfo, tabinfolist_fragment).addToBackStack(null).commit();

        Log.i("TEST_LIFECYCLE", "onStart Fragment TabInfo");
        return root;
    }
    @Override
    public void onStart() {
        super.onStart();
        Log.i("TEST_LIFECYCLE", "onStart Fragment TabInfo");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("TEST_LIFECYCLE", "onResume Fragment TabInfo");
    }
}
