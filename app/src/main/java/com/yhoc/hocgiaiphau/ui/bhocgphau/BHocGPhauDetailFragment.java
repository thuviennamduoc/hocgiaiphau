package com.yhoc.hocgiaiphau.ui.bhocgphau;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.github.barteksc.pdfviewer.listener.OnTapListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.shockwave.pdfium.PdfDocument;
import com.yhoc.hocgiaiphau.MainActivityViewModel;
import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.ui.tab.TabBasicFragment;

import java.util.List;

public class BHocGPhauDetailFragment extends Fragment implements OnPageChangeListener, OnLoadCompleteListener {
    public String SAMPLE_FILE = "hetieuhoa_coban.pdf";
    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;

    public static final String TAG_HGP_BHocGPhauDetailFragment = "HGP_BHocGPhauDetailFragment";
    private BHocGPhauDetailViewModel bhocgphaudetailmodel;
    private MainActivityViewModel mainactivitymodel;

    View root;
    private AdView mAdView;

    public interface TaskCallbacks {
        void updateUI();
    }

    private TaskCallbacks mCallbacks;

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_bhocgphau_detail, container, false);

        bhocgphaudetailmodel = ViewModelProviders.of(requireActivity()).get(BHocGPhauDetailViewModel.class);
        mainactivitymodel = ViewModelProviders.of(requireActivity()).get(MainActivityViewModel.class);

        pdfFileName = bhocgphaudetailmodel.getValueFromNameFile();
        pdfView = root.findViewById(R.id.pdfViewBHocGPhauDetail);
        mAdView = root.findViewById(R.id.adViewBHocDetail);


        displayFromAsset(pdfFileName);

        MobileAds.initialize(getContext());
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Log.e("ADS", "onAdFailedToLoad " + i);
            }
        });



        Log.i("", "onCreateView BHocGPhauDetailFragment");
        setRetainInstance(true);
        return root;
    }

    private void displayFromAsset(String assetFileName) {
        pdfView.fromAsset(assetFileName)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onTap(new OnTapListener() {
                    @Override
                    public boolean onTap(MotionEvent e) {
                        Activity activity = getActivity();
                        mCallbacks = (BHocGPhauDetailFragment.TaskCallbacks) activity;
                        mCallbacks.updateUI();
                        return true;
                    }
                })
                .onRender(new OnRenderListener() {
                    @Override
                    public void onInitiallyRendered(int nbPages) {
                        pdfView.fitToWidth(0);
                    }
                })
                .onLoad(this)
                .load();


    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        //getActivity().setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
        Toast.makeText(this.getActivity(), String.format("%s %s / %s", pdfFileName, page + 1, pageCount), Toast.LENGTH_LONG);
    }

    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {
            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }
}

