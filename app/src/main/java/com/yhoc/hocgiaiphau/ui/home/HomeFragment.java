package com.yhoc.hocgiaiphau.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.lib.BHocGPhauRecyclerAdapter;
import com.yhoc.hocgiaiphau.lib.BaiHocGiaiPhauAdapter;
import com.yhoc.hocgiaiphau.libdatabase.BaiHocGiaiPhau;
import com.yhoc.hocgiaiphau.libdatabase.DbHocGiaiPhau;
import com.yhoc.hocgiaiphau.ui.bhocgphau.BHocGPhauFragment;

import java.util.List;

public class HomeFragment extends Fragment implements BHocGPhauRecyclerAdapter.ItemClickListener {
    public static String TAG_HOMEFRAGMENT = "HomeFragment";

    private RecyclerView rcvListBaiHoc;
    BHocGPhauRecyclerAdapter adapterRCVBHocGPhau;

    private ListView lvContact;
    private ListView lvDanhMucHome;
    private SwipeMenuListView swipeMenuListView;
    List<BaiHocGiaiPhau> arrBaiHoc;
    Toolbar mActionBarToolbar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.fragment_home, container, false);
        //lvContact = (ListView) root.findViewById(R.id.lv_Contact);
        rcvListBaiHoc = root.findViewById(R.id.lv_Contact);
        init();
        //initUIlistview();
        initUIlistRecyclerView();

        Log.i("TEST_LIFECYCLE", "onCreate Fragment Home");
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("TEST_LIFECYCLE", "onStart Fragment Home");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("TEST_LIFECYCLE", "onResume Fragment Home");
    }

    private void initUIlistRecyclerView() {
        rcvListBaiHoc.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterRCVBHocGPhau = new BHocGPhauRecyclerAdapter(getContext(), arrBaiHoc);
        adapterRCVBHocGPhau.setClickListener(this);
        rcvListBaiHoc.setAdapter(adapterRCVBHocGPhau);

    }

    @Override
    public void onItemClick(View view, int position) {
        BHocGPhauFragment fragment = new BHocGPhauFragment();
        Bundle bundle = new Bundle();
        bundle.putString(BHocGPhauFragment.KEY_TENFILE_ITEM, arrBaiHoc.get(position).getDuongdanfileBHocGPhau());
        fragment.setArguments(bundle);
        Log.i(TAG_HOMEFRAGMENT, "Truyen data ten file" + arrBaiHoc.get(position).getDuongdanfileBHocGPhau());


        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        //FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        //fragmentTransaction.replace(R.id.frame_layout, fragment, MainActivity.TAG_BHocGPhauFragment).addToBackStack(null).commit();
    }

    private void initUIlistview() {

        BaiHocGiaiPhauAdapter baihocAdapter = new BaiHocGiaiPhauAdapter(this.getActivity(), R.layout.list_item_views, arrBaiHoc);
        lvContact.setAdapter(baihocAdapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                BHocGPhauFragment fragment = new BHocGPhauFragment();
                Bundle bundle = new Bundle();
                bundle.putString(BHocGPhauFragment.KEY_TENFILE_ITEM, arrBaiHoc.get(i).getDuongdanfileBHocGPhau());
                fragment.setArguments(bundle);
                Log.i(TAG_HOMEFRAGMENT, "Truyen data ten file" + arrBaiHoc.get(i).getDuongdanfileBHocGPhau());


                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.frame_layout, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                //FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                //fragmentTransaction.replace(R.id.frame_layout, fragment, MainActivity.TAG_BHocGPhauFragment).addToBackStack(null).commit();

            }
        });
    }

    public void init() {
        DbHocGiaiPhau dbBHocGPhauManager = new DbHocGiaiPhau(getActivity());
        int soluong = dbBHocGPhauManager.getBHocGPhauCount();
        if (soluong == 0) {
            dbBHocGPhauManager.addBaiHocGiaiPhau(new BaiHocGiaiPhau(1, "Hệ xương khớp", 5, "hexuongkhop"));
            dbBHocGPhauManager.addBaiHocGiaiPhau(new BaiHocGiaiPhau(2, "Hệ cơ", 5, "heco"));
            dbBHocGPhauManager.addBaiHocGiaiPhau(new BaiHocGiaiPhau(3, "Hệ tuần hoàn", 5, "hetuanhoan"));
            dbBHocGPhauManager.addBaiHocGiaiPhau(new BaiHocGiaiPhau(4, "Hệ hô hấp", 5, "hehohap"));
            dbBHocGPhauManager.addBaiHocGiaiPhau(new BaiHocGiaiPhau(5, "Hệ tiêu hóa", 5, "hetieuhoa"));
            dbBHocGPhauManager.addBaiHocGiaiPhau(new BaiHocGiaiPhau(6, "Hệ tiết niệu", 5, "hetietnieu"));
            dbBHocGPhauManager.addBaiHocGiaiPhau(new BaiHocGiaiPhau(7, "Hệ thần kinh", 5, "hethankinh"));
            dbBHocGPhauManager.addBaiHocGiaiPhau(new BaiHocGiaiPhau(8, "Hệ nội tiết", 5, "henoitiet"));
        }
        arrBaiHoc = dbBHocGPhauManager.getAllBHocGPhau();
    }


}
