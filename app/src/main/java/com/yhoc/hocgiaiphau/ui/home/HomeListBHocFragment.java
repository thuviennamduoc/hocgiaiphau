package com.yhoc.hocgiaiphau.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.lib.CustomExpandableListAdapter;
import com.yhoc.hocgiaiphau.lib.ExpandableListData;
import com.yhoc.hocgiaiphau.lib.ListDataItem;
import com.yhoc.hocgiaiphau.ui.bhocgphau.BHocGPhauDetailFragment;
import com.yhoc.hocgiaiphau.ui.bhocgphau.BHocGPhauDetailViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeListBHocFragment extends Fragment {

    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<ListDataItem>> expandableListDetail;


    BHocGPhauDetailViewModel bhocgphaudetailmodel;
    public static final String TAG_HGP_HomeListBHocFragment = "HGP_HomeListBHocFragment";

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_homelistbhoc, container, false);


        expandableListView = root.findViewById(R.id.expandableListView);

        expandableListDetail = ExpandableListData.getData();

        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(getActivity(), expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                /*Toast.makeText(getContext(),
                        expandableListTitle.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT).show();*/
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                /*Toast.makeText(getContext(),
                        expandableListTitle.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT).show();*/

            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                /*Toast.makeText(
                        getContext(),
                        expandableListTitle.get(groupPosition)
                                + " -> "
                                + expandableListDetail.get(
                                expandableListTitle.get(groupPosition)).get(
                                childPosition), Toast.LENGTH_SHORT
                ).show();*/
                bhocgphaudetailmodel = ViewModelProviders.of(requireActivity()).get(BHocGPhauDetailViewModel.class);
                ListDataItem dataItem = (ListDataItem) expandableListDetail.get(expandableListTitle.get(groupPosition)).get(childPosition);
                String nameFile = dataItem.getNamePath();

                if(!nameFile.equals("")){
                    bhocgphaudetailmodel.setValueForNameFile(nameFile);
                    BHocGPhauDetailFragment bhocgphaudetail_fragment = new BHocGPhauDetailFragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, bhocgphaudetail_fragment).addToBackStack(null).commit();
                }else{
                    ListBHocFragment lishbhocfragment = new ListBHocFragment();
                    FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.frame_layout, lishbhocfragment).addToBackStack(null).commit();
                }



                return false;
            }
        });

        Log.i(TAG_HGP_HomeListBHocFragment, "onCreateView HomeListBHocFragment");
        return root;
    }
}
