package com.yhoc.hocgiaiphau.ui.bhocgphau;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class BHocGPhauViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;

    public MutableLiveData<Integer> getmValue() {
        return mValue;
    }

    public void setmValue(MutableLiveData<Integer> mValue) {
        this.mValue = mValue;
    }

    private MutableLiveData<Integer> mValue;

    public BHocGPhauViewModel(Application application) {
        super(application);
        mText = new MutableLiveData<>();
        mText.setValue("This is slideshow fragment");

        mValue = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }

    public void duaGiaTri(int giatri) {
        mValue.setValue(giatri);
    }
}