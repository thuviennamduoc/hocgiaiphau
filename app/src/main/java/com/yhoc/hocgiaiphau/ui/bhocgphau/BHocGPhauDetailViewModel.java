package com.yhoc.hocgiaiphau.ui.bhocgphau;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

public class BHocGPhauDetailViewModel extends AndroidViewModel {


    public MutableLiveData<String> getmNameFile() {
        return mNameFile;
    }

    public void setmNameFile(MutableLiveData<String> mNameFile) {
        this.mNameFile = mNameFile;
    }

    private MutableLiveData<String> mNameFile;

    public BHocGPhauDetailViewModel(Application application) {
        super(application);
        mNameFile = new MutableLiveData<>();
    }

    public void setValueForNameFile(String value) {
        mNameFile.setValue(value);
    }

    public String getValueFromNameFile() {
        return mNameFile.getValue();
    }
}