package com.yhoc.hocgiaiphau.ui.tab;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.yhoc.hocgiaiphau.R;
import com.yhoc.hocgiaiphau.ui.bhocgphau.BHocGPhauViewModel;

public class TabsSectionAdapter extends FragmentStatePagerAdapter {
    public final String TAG_TABSSECTIONADAPTER = "TabsSectionAdapter";
    private BHocGPhauViewModel bhocgiaiphauViewModel;
    TabBasicFragment tabbasicFragment;
    TabPreFragment tabpreFragment;



    String tenFile = "";

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_pre, R.string.tab_pro, R.string.tab_basic};
    private final Context mContext;

    public TabsSectionAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;

    }
    public void setTenFile(String tenfile){
        this.tenFile = tenfile;
        //Log.i(TAG_TABSSECTIONADAPTER, tabbasicviewmodel.layGiaTriTenFile() + tabpreviewmodel.layGiaTriTenFile() + tabproviewmodel.layGiaTriTenFile());
    }



    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position){
            case 0:
                return new TabBasicFragment();
            case 1:
                return new TabPreFragment();
            case 2:
                return new TabInfoFragment();
        }
        return null;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        return TAB_TITLES.length;
    }
}

