package com.yhoc.hocgiaiphau;

import android.app.Application;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.yhoc.hocgiaiphau.utils.ForceUpdateChecker;

import java.util.HashMap;

public class MyApplication extends Application {
    private static final String TAG = MyApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        // set in-app defaults
        HashMap<String, Object> remoteConfigDefaults = new HashMap();
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_FORCE_UPDATE_REQUIRED, false);
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_NEWEST_VERSION_CODE, 100);
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_URL,
                "https://play.google.com/store/apps/details?id=com.yhoc.hocgiaphau");

        firebaseRemoteConfig.setDefaultsAsync(remoteConfigDefaults);
    }
}
